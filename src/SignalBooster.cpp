#include "plugin.hpp"

struct SignalBooster : Module {
	enum ParamIds {
        BOOST_PARAM,
		NUM_PARAMS
	};
	enum InputIds {
		SIGNAL_INPUT,
		NUM_INPUTS
	};
	enum OutputIds {
		BOOSTED_OUTPUT,
        BOOSTED_OUTPUT_2X,
        BOOSTED_OUTPUT_5X,
        NUM_OUTPUTS
	};
	enum LightIds {
		NUM_LIGHTS
	};

	SignalBooster() {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);
		configParam(BOOST_PARAM, 0.f, 5.f, 0.f, "");
	}

    void process(const ProcessArgs& args) override{
        //get sample of input signal
        float sample = inputs[SIGNAL_INPUT].getVoltage();
        //get knob amount
        float boost_val = params[BOOST_PARAM].getValue();
        //boost and send out sample signal, 1x, 2x, 5x
        outputs[BOOSTED_OUTPUT].setVoltage(boost_val * sample);
        outputs[BOOSTED_OUTPUT_2X].setVoltage((boost_val * 2) * sample);
        outputs[BOOSTED_OUTPUT_5X].setVoltage((boost_val * 5)  * sample);
    }
};

struct SignalBoosterWidget : ModuleWidget {
	SignalBoosterWidget(SignalBooster* module) {
		setModule(module);
		setPanel(APP->window->loadSvg(asset::plugin(pluginInstance, "res/SignalBooster.svg")));

		addChild(createWidget<ScrewSilver>(Vec(RACK_GRID_WIDTH, 0)));
		addChild(createWidget<ScrewSilver>(Vec(box.size.x - 2 * RACK_GRID_WIDTH, 0)));
		addChild(createWidget<ScrewSilver>(Vec(RACK_GRID_WIDTH, RACK_GRID_HEIGHT - RACK_GRID_WIDTH)));
		addChild(createWidget<ScrewSilver>(Vec(box.size.x - 2 * RACK_GRID_WIDTH, RACK_GRID_HEIGHT - RACK_GRID_WIDTH)));

		addParam(createParamCentered<RoundBlackKnob>(mm2px(Vec(15.24, 46.063)), module, SignalBooster::BOOST_PARAM));
		//create inoputs
		addInput(createInputCentered<PJ301MPort>(mm2px(Vec(15.24, 70.713)), module, SignalBooster::SIGNAL_INPUT));

		//create outputs
		addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.24, 94.713)), module, SignalBooster::BOOSTED_OUTPUT));
        addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.24, 104.713)), module, SignalBooster::BOOSTED_OUTPUT_2X));
        addOutput(createOutputCentered<PJ301MPort>(mm2px(Vec(15.24, 114.713)), module, SignalBooster::BOOSTED_OUTPUT_5X));
	}
};

Model* modelSignalBooster = createModel<SignalBooster, SignalBoosterWidget>("SignalBooster");