# Sunset Signals


| Plugin Name | Plugin Description | License | Screenshot |
| ----------- | ----------- | ----------- | ----------- |
| Signal Booster | Takes in a Signal and multiplies it using the knob from 0.0 - 2.0.  It also has 1x,2x,5x outputs. | [MIT](https://opensource.org/licenses/MIT)| ![sceenshot](docs/images/booster_screenshot.png) |
